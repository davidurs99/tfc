from flask import Flask, render_template, url_for, request, abort
import datetime

import stripe, os, sqlalchemy

app = Flask(__name__)

app.config['STRIPE_PUBLIC_KEY'] = os.environ["PUBLIC_KEY"]
app.config['STRIPE_SECRET_KEY'] = os.environ["SECRET_KEY"]
price_id = os.environ["PRICE_ID"]
endpoint_secret = os.environ["WEBHOOK_SECRET"]

stripe.api_key = app.config['STRIPE_SECRET_KEY']

def init_connection_engine():
    db_config = {
        "pool_size": 5,
        "max_overflow": 2,
        "pool_timeout": 30,  # 30 seconds
        "pool_recycle": 1800  # 30 minutes
    }

    if os.environ.get("DB_HOST"):
        return init_tcp_connection_engine(db_config)
    else:
        return init_tcp_connection_engine(db_config)


def init_tcp_connection_engine(db_config):
    # [START cloud_sql_mysql_sqlalchemy_create_tcp]
    # Remember - storing secrets in plaintext is potentially unsafe. Consider using
    # something like https://cloud.google.com/secret-manager/docs/overview to help keep
    # secrets secret.
    db_user = os.environ["DB_USER"]
    db_pass = os.environ["DB_PASS"]
    db_name = os.environ["DB_NAME"]
    cloud_sql_connection_name = os.environ["SQL_CONNECTION_NAME"]
    db_socket_dir = os.environ.get("DB_SOCKET_DIR", "/cloudsql")

    pool = sqlalchemy.create_engine(
        # Equivalent URL:
        # mysql+pymysql://<db_user>:<db_pass>@/<db_name>?unix_socket=<socket_path>/<cloud_sql_instance_name>
        sqlalchemy.engine.url.URL(
            drivername="mysql+pymysql",
            username=db_user,  # e.g. "my-database-user"
            password=db_pass,  # e.g. "my-database-password"
            database=db_name,  # e.g. "my-database-name"
            query={
                "unix_socket": "{}/{}".format(
                    db_socket_dir,  # e.g. "/cloudsql"
                    cloud_sql_connection_name)  # i.e "<PROJECT-NAME>:<INSTANCE-REGION>:<INSTANCE-NAME>"
            }
        ),
        **db_config
    )

    return pool

# This global variable is declared with a value of `None`, instead of calling
# `init_connection_engine()` immediately, to simplify testing. In general, it
# is safe to initialize your database connection pool when your script starts
# -- there is no need to wait for the first request.
db = None


@app.route('/')
def main():    
    return render_template(
        'index.html', 
    )

@app.before_first_request
def create_tables():
    global db
    db = db or init_connection_engine()

    with db.connect() as conn:
        conn.execute(
            "CREATE TABLE IF NOT EXISTS orders "
            "( id VARCHAR(255) NOT NULL, customer_email VARCHAR ( 255 ) NOT NULL, "
            "customer_name VARCHAR ( 50 ) NOT NULL,"
            "city VARCHAR ( 50 ) NOT NULL,"
            "country VARCHAR ( 50 ) NOT NULL,"
            "address_line_1 VARCHAR ( 255 ) NOT NULL,"
            "address_line_2 VARCHAR ( 255 ),"
            "postal_code VARCHAR ( 255 ) NOT NULL,"
            "quantity VARCHAR ( 5 ) NOT NULL,"
            "amount_total VARCHAR ( 50 ) NOT NULL,"
            "created_on TIMESTAMP NOT NULL, PRIMARY KEY (id) );"
        )

@app.route('/stripe_pay')
def stripe_pay():
    session = stripe.checkout.Session.create(
        payment_method_types=['card'],
        line_items=[{
            'price': price_id,
            'quantity': 1,
        }],
        shipping_address_collection={
            'allowed_countries': ['GB']
        },
        mode='payment',
        success_url=url_for('thanks', _external=True) + '?session_id={CHECKOUT_SESSION_ID}',
        cancel_url=url_for('main', _external=True),
    )
    return {
        'checkout_session_id': session['id'], 
        'checkout_public_key': app.config['STRIPE_PUBLIC_KEY']
    }

@app.route('/thanks')
def thanks():
    return render_template('thanks.html')

@app.route('/stripe_webhook', methods=['POST'])
def stripe_webhook():
    print('WEBHOOK CALLED')

    if request.content_length > 1024 * 1024:
        print('REQUEST TOO BIG')
        abort(400)
    payload = request.get_data()
    sig_header = request.environ.get('HTTP_STRIPE_SIGNATURE')
    endpoint_secret = endpoint_secret
    event = None

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, endpoint_secret
        )
    except ValueError as e:
        # Invalid payload
        print('INVALID PAYLOAD')
        return {}, 400
    except stripe.error.SignatureVerificationError as e:
        # Invalid signature
        print('INVALID SIGNATURE')
        return {}, 400

    # Handle the checkout.session.completed event
    if event['type'] == 'checkout.session.completed':
        session = event['data']['object']
        detailed_session = stripe.checkout.Session.retrieve(
            session['id'],
            expand=['customer']
        )

        print(detailed_session)
        

        stmt = sqlalchemy.text(
            "INSERT INTO orders (id, customer_email, customer_name, city, country, address_line_1, address_line_2, postal_code, quantity, amount_total, created_on) \
            VALUES (:id, :email, :name, :city, :country, :line1, :line2, :postal_code, :quantity, :amount, :time)"
        )


        try:
        # Using a with statement ensures that the connection is always released
        # back into the pool at the end of statement (even if an error occurs)
            with db.connect() as conn:
                conn.execute(stmt, id = detailed_session['id'], name = detailed_session['shipping']['name'], email = detailed_session['customer']['email'], 
                city = detailed_session['shipping']['address']['city'], country = detailed_session['shipping']['address']['country'], 
                line1 = detailed_session['shipping']['address']['line1'], line2 = detailed_session['shipping']['address']['line2'], 
                postal_code = detailed_session['shipping']['address']['postal_code'], quantity = "1",
                amount = detailed_session['amount_total'], time = datetime.datetime.utcnow())
        except Exception as e:
            # If something goes wrong, handle the error in this section. This might
            # involve retrying or adjusting parameters depending on the situation.
            # [START_EXCLUDE]
            print('exception')
            print(e)
            # [END_EXCLUDE]
    # [END cloud_sql_mysql_sqlalchemy_connection]

    return {}

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80)
